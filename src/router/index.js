import { createRouter, createWebHistory } from 'vue-router'
import MainPage from "@/pages/mainPage/MainPage";
import CharacterPage from "@/pages/characterPage/CharacterPage";

const routes = [
  {
    path: '/',
    name: 'Main',
    component: MainPage
  },
  {
    path: '/character/:id',
    name: 'Character',
    component: CharacterPage
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
