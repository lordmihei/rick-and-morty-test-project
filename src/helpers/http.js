import Axios from "axios";

class Http {
    constructor() {
        this.accessToken = localStorage.getItem('accessToken');
        this.apiUrlPrefix = '/api/';
    }

    get(url, data, options = {}) {
        let paramsString = '';
        if (data) {
            let keyValuePairs = [];
            for (let key in data) {
                if (Object.prototype.hasOwnProperty.call(data, key)) {
                    keyValuePairs.push(`${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`);
                }
            }
            paramsString = '?' + keyValuePairs.join('&');
        }
        return this._send(url + paramsString, 'get', data, options);
    }

    post(url, data, options = {}) {
        return this._send(url, 'post', data, options);
    }

    delete(url, data, options = {}) {
        return this._send(url, 'delete', data, options);
    }

    put(url, data, options = {}) {
        return this._send(url, 'put', data, options);
    }

    send(method, url, data, options = {}) {
        method = method.toLowerCase();

        return this._send(url, method, data, options);
    }

    _send = async (url, method, data, options) => {
        let requestOptions = this.getRequestOptions(method, data, options);
        let response = null;

        try {
            response = await Axios({
                url: "https://rickandmortyapi.com" + this.apiUrlPrefix + url,
                ...requestOptions
            });
        } catch (e) {
            if (options.throwOnError) {
                throw e;
            } else {
                this.showHttpError(e);
            }
        }

        return response && response.data;
    };

    getRequestOptions(method, data) {
        let requestOptions = {
            method: method,
            headers: {
                'Accept': 'application/json',
                'X-Requested-With': 'XMLHttpRequest'
            },
        };

        if (method !== 'get' && method !== 'head') {
            requestOptions.data = data;
        }

        requestOptions.headers['Content-Type'] = 'application/json';

        if (this.accessToken) {
            requestOptions.headers['X-Authorization'] = 'Bearer ' + this.accessToken;
        }

        return requestOptions;
    }

    /**
     * @param e Exception from Axios
     */
    showHttpError(e) {
        let message;
        const responseStatus = Number(e.response.status);

        switch (responseStatus) {
            case 500:
                message = 'Произошла внутренняя ошибка сервера при обработке запроса.' +
                    ' Пожалуйста, обратитесь в техподдержку';
                break;
            case 404:
                message = 'Похоже что запрошенного ресурса на сервере не существует.' +
                    ' Пожалуйста, обратитесь в техподдержку';
                break;
            case 504:
                message = 'Не удалось соединиться с сервером. Пожалуйста, проверьте ваше соединение с интернетом.';
                break;
            case 400:
                message = 'Похоже что в запросе не хватает параметров для его корректной обработки сервером.' +
                    ' Пожалуйста, обратитесь в техподдержку';
                break;
            default:
                message = `При запросе на сервер произошла ошибка. Статус ответа - ${responseStatus}`;
        }
        if (e.response.data) {
            message = e.response.data;
        } else {
            message = `Ошибка при соединении с сервером (HTTP ${e.response.status})`;
        }

        console.log(message);
    }
}

export default new Http();